// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: branch_product.proto

package branch_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// The request message containing the user's name.
type CreateBranchProduct struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ProductId string `protobuf:"bytes,1,opt,name=ProductId,proto3" json:"ProductId,omitempty"`
	BranchId  string `protobuf:"bytes,2,opt,name=BranchId,proto3" json:"BranchId,omitempty"`
	Count     int64  `protobuf:"varint,3,opt,name=Count,proto3" json:"Count,omitempty"`
}

func (x *CreateBranchProduct) Reset() {
	*x = CreateBranchProduct{}
	if protoimpl.UnsafeEnabled {
		mi := &file_branch_product_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateBranchProduct) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateBranchProduct) ProtoMessage() {}

func (x *CreateBranchProduct) ProtoReflect() protoreflect.Message {
	mi := &file_branch_product_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateBranchProduct.ProtoReflect.Descriptor instead.
func (*CreateBranchProduct) Descriptor() ([]byte, []int) {
	return file_branch_product_proto_rawDescGZIP(), []int{0}
}

func (x *CreateBranchProduct) GetProductId() string {
	if x != nil {
		return x.ProductId
	}
	return ""
}

func (x *CreateBranchProduct) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *CreateBranchProduct) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

type GetProductRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BranchId  string `protobuf:"bytes,1,opt,name=BranchId,proto3" json:"BranchId,omitempty"`
	ProductId string `protobuf:"bytes,2,opt,name=ProductId,proto3" json:"ProductId,omitempty"`
}

func (x *GetProductRequest) Reset() {
	*x = GetProductRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_branch_product_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetProductRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetProductRequest) ProtoMessage() {}

func (x *GetProductRequest) ProtoReflect() protoreflect.Message {
	mi := &file_branch_product_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetProductRequest.ProtoReflect.Descriptor instead.
func (*GetProductRequest) Descriptor() ([]byte, []int) {
	return file_branch_product_proto_rawDescGZIP(), []int{1}
}

func (x *GetProductRequest) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *GetProductRequest) GetProductId() string {
	if x != nil {
		return x.ProductId
	}
	return ""
}

type BranchProduct struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id        string `protobuf:"bytes,1,opt,name=Id,proto3" json:"Id,omitempty"`
	ProductId string `protobuf:"bytes,2,opt,name=ProductId,proto3" json:"ProductId,omitempty"`
	BranchId  string `protobuf:"bytes,3,opt,name=BranchId,proto3" json:"BranchId,omitempty"`
	Count     int64  `protobuf:"varint,4,opt,name=Count,proto3" json:"Count,omitempty"`
	CreatedAt string `protobuf:"bytes,5,opt,name=Created_at,json=CreatedAt,proto3" json:"Created_at,omitempty"`
	UpdatedAt string `protobuf:"bytes,6,opt,name=Updated_at,json=UpdatedAt,proto3" json:"Updated_at,omitempty"`
}

func (x *BranchProduct) Reset() {
	*x = BranchProduct{}
	if protoimpl.UnsafeEnabled {
		mi := &file_branch_product_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BranchProduct) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BranchProduct) ProtoMessage() {}

func (x *BranchProduct) ProtoReflect() protoreflect.Message {
	mi := &file_branch_product_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BranchProduct.ProtoReflect.Descriptor instead.
func (*BranchProduct) Descriptor() ([]byte, []int) {
	return file_branch_product_proto_rawDescGZIP(), []int{2}
}

func (x *BranchProduct) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *BranchProduct) GetProductId() string {
	if x != nil {
		return x.ProductId
	}
	return ""
}

func (x *BranchProduct) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *BranchProduct) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *BranchProduct) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *BranchProduct) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type GetAllBranchProductRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Page  int64 `protobuf:"varint,1,opt,name=Page,proto3" json:"Page,omitempty"`
	Limit int64 `protobuf:"varint,2,opt,name=Limit,proto3" json:"Limit,omitempty"`
}

func (x *GetAllBranchProductRequest) Reset() {
	*x = GetAllBranchProductRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_branch_product_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllBranchProductRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllBranchProductRequest) ProtoMessage() {}

func (x *GetAllBranchProductRequest) ProtoReflect() protoreflect.Message {
	mi := &file_branch_product_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllBranchProductRequest.ProtoReflect.Descriptor instead.
func (*GetAllBranchProductRequest) Descriptor() ([]byte, []int) {
	return file_branch_product_proto_rawDescGZIP(), []int{3}
}

func (x *GetAllBranchProductRequest) GetPage() int64 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *GetAllBranchProductRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

type GetAllBranchProductResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BranchProducts []*BranchProduct `protobuf:"bytes,1,rep,name=BranchProducts,proto3" json:"BranchProducts,omitempty"`
	Count          int64            `protobuf:"varint,2,opt,name=Count,proto3" json:"Count,omitempty"`
}

func (x *GetAllBranchProductResponse) Reset() {
	*x = GetAllBranchProductResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_branch_product_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllBranchProductResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllBranchProductResponse) ProtoMessage() {}

func (x *GetAllBranchProductResponse) ProtoReflect() protoreflect.Message {
	mi := &file_branch_product_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllBranchProductResponse.ProtoReflect.Descriptor instead.
func (*GetAllBranchProductResponse) Descriptor() ([]byte, []int) {
	return file_branch_product_proto_rawDescGZIP(), []int{4}
}

func (x *GetAllBranchProductResponse) GetBranchProducts() []*BranchProduct {
	if x != nil {
		return x.BranchProducts
	}
	return nil
}

func (x *GetAllBranchProductResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

var File_branch_product_proto protoreflect.FileDescriptor

var file_branch_product_proto_rawDesc = []byte{
	0x0a, 0x14, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0e, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x0c, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x22, 0x65, 0x0a, 0x13, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x42, 0x72,
	0x61, 0x6e, 0x63, 0x68, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x12, 0x1c, 0x0a, 0x09, 0x50,
	0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09,
	0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x12, 0x1a, 0x0a, 0x08, 0x42, 0x72, 0x61,
	0x6e, 0x63, 0x68, 0x49, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x42, 0x72, 0x61,
	0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x22, 0x4d, 0x0a, 0x11, 0x47,
	0x65, 0x74, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x1a, 0x0a, 0x08, 0x42, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x08, 0x42, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x1c, 0x0a, 0x09,
	0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x09, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x22, 0xad, 0x01, 0x0a, 0x0d, 0x42,
	0x72, 0x61, 0x6e, 0x63, 0x68, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x12, 0x0e, 0x0a, 0x02,
	0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x49, 0x64, 0x12, 0x1c, 0x0a, 0x09,
	0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x09, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x12, 0x1a, 0x0a, 0x08, 0x42, 0x72,
	0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x42, 0x72,
	0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x1d, 0x0a, 0x0a,
	0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x09, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x09, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x46, 0x0a, 0x1a, 0x47, 0x65,
	0x74, 0x41, 0x6c, 0x6c, 0x42, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63,
	0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x50, 0x61, 0x67, 0x65,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x04, 0x50, 0x61, 0x67, 0x65, 0x12, 0x14, 0x0a, 0x05,
	0x4c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x4c, 0x69, 0x6d,
	0x69, 0x74, 0x22, 0x7a, 0x0a, 0x1b, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x42, 0x72, 0x61, 0x6e,
	0x63, 0x68, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x12, 0x45, 0x0a, 0x0e, 0x42, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x50, 0x72, 0x6f, 0x64, 0x75,
	0x63, 0x74, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1d, 0x2e, 0x62, 0x72, 0x61, 0x6e,
	0x63, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x42, 0x72, 0x61, 0x6e, 0x63,
	0x68, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x52, 0x0e, 0x42, 0x72, 0x61, 0x6e, 0x63, 0x68,
	0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x43, 0x6f, 0x75, 0x6e,
	0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x32, 0xeb,
	0x03, 0x0a, 0x14, 0x42, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74,
	0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x49, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x12, 0x23, 0x2e, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x42, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x50,
	0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x1a, 0x18, 0x2e, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x49, 0x64, 0x52, 0x65, 0x71, 0x52, 0x65, 0x73,
	0x22, 0x00, 0x12, 0x49, 0x0a, 0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x1d, 0x2e, 0x62,
	0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x42, 0x72,
	0x61, 0x6e, 0x63, 0x68, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x1a, 0x1e, 0x2e, 0x62, 0x72,
	0x61, 0x6e, 0x63, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x22, 0x00, 0x12, 0x40, 0x0a,
	0x03, 0x47, 0x65, 0x74, 0x12, 0x18, 0x2e, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x49, 0x64, 0x52, 0x65, 0x71, 0x52, 0x65, 0x73, 0x1a, 0x1d,
	0x2e, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x42, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x22, 0x00, 0x12,
	0x44, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x18, 0x2e, 0x62, 0x72, 0x61, 0x6e,
	0x63, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x49, 0x64, 0x52, 0x65, 0x71,
	0x52, 0x65, 0x73, 0x1a, 0x1e, 0x2e, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x53, 0x74, 0x72,
	0x69, 0x6e, 0x67, 0x22, 0x00, 0x12, 0x63, 0x0a, 0x06, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x12,
	0x2a, 0x2e, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x42, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x50, 0x72, 0x6f,
	0x64, 0x75, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x2b, 0x2e, 0x62, 0x72,
	0x61, 0x6e, 0x63, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x47, 0x65, 0x74,
	0x41, 0x6c, 0x6c, 0x42, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74,
	0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x50, 0x0a, 0x0a, 0x47, 0x65,
	0x74, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x12, 0x21, 0x2e, 0x62, 0x72, 0x61, 0x6e, 0x63,
	0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x47, 0x65, 0x74, 0x50, 0x72, 0x6f,
	0x64, 0x75, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1d, 0x2e, 0x62, 0x72,
	0x61, 0x6e, 0x63, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x42, 0x72, 0x61,
	0x6e, 0x63, 0x68, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x22, 0x00, 0x42, 0x19, 0x5a, 0x17,
	0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_branch_product_proto_rawDescOnce sync.Once
	file_branch_product_proto_rawDescData = file_branch_product_proto_rawDesc
)

func file_branch_product_proto_rawDescGZIP() []byte {
	file_branch_product_proto_rawDescOnce.Do(func() {
		file_branch_product_proto_rawDescData = protoimpl.X.CompressGZIP(file_branch_product_proto_rawDescData)
	})
	return file_branch_product_proto_rawDescData
}

var file_branch_product_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_branch_product_proto_goTypes = []interface{}{
	(*CreateBranchProduct)(nil),         // 0: branch_service.CreateBranchProduct
	(*GetProductRequest)(nil),           // 1: branch_service.GetProductRequest
	(*BranchProduct)(nil),               // 2: branch_service.BranchProduct
	(*GetAllBranchProductRequest)(nil),  // 3: branch_service.GetAllBranchProductRequest
	(*GetAllBranchProductResponse)(nil), // 4: branch_service.GetAllBranchProductResponse
	(*IdReqRes)(nil),                    // 5: branch_service.IdReqRes
	(*ResponseString)(nil),              // 6: branch_service.ResponseString
}
var file_branch_product_proto_depIdxs = []int32{
	2, // 0: branch_service.GetAllBranchProductResponse.BranchProducts:type_name -> branch_service.BranchProduct
	0, // 1: branch_service.BranchProductService.Create:input_type -> branch_service.CreateBranchProduct
	2, // 2: branch_service.BranchProductService.Update:input_type -> branch_service.BranchProduct
	5, // 3: branch_service.BranchProductService.Get:input_type -> branch_service.IdReqRes
	5, // 4: branch_service.BranchProductService.Delete:input_type -> branch_service.IdReqRes
	3, // 5: branch_service.BranchProductService.GetAll:input_type -> branch_service.GetAllBranchProductRequest
	1, // 6: branch_service.BranchProductService.GetProduct:input_type -> branch_service.GetProductRequest
	5, // 7: branch_service.BranchProductService.Create:output_type -> branch_service.IdReqRes
	6, // 8: branch_service.BranchProductService.Update:output_type -> branch_service.ResponseString
	2, // 9: branch_service.BranchProductService.Get:output_type -> branch_service.BranchProduct
	6, // 10: branch_service.BranchProductService.Delete:output_type -> branch_service.ResponseString
	4, // 11: branch_service.BranchProductService.GetAll:output_type -> branch_service.GetAllBranchProductResponse
	2, // 12: branch_service.BranchProductService.GetProduct:output_type -> branch_service.BranchProduct
	7, // [7:13] is the sub-list for method output_type
	1, // [1:7] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_branch_product_proto_init() }
func file_branch_product_proto_init() {
	if File_branch_product_proto != nil {
		return
	}
	file_branch_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_branch_product_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateBranchProduct); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_branch_product_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetProductRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_branch_product_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BranchProduct); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_branch_product_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAllBranchProductRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_branch_product_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAllBranchProductResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_branch_product_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_branch_product_proto_goTypes,
		DependencyIndexes: file_branch_product_proto_depIdxs,
		MessageInfos:      file_branch_product_proto_msgTypes,
	}.Build()
	File_branch_product_proto = out.File
	file_branch_product_proto_rawDesc = nil
	file_branch_product_proto_goTypes = nil
	file_branch_product_proto_depIdxs = nil
}
