package grpc_client

import (
	"fmt"

	"sale/config"
	"sale/genproto/branch_service"
	"sale/genproto/catalog_service"
	"sale/genproto/staff_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// GrpcClientI ...
type GrpcClientI interface {
	BranchService() branch_service.BranchServiceClient
	TarifService() staff_service.TarifServerClient
	StaffService() staff_service.StaffServerClient
	// CatagoryService() catalog_service.CategoryServiceClient
	// ProductService() catalog_service.ProductServiceClient
}

// GrpcClient ...
type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

// New ...
func New(cfg config.Config) (*GrpcClient, error) {

	connStaff, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.StaffServiceHost, cfg.StaffServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("staff service dial host: %s port:%d err: %s",
			cfg.StaffServiceHost, cfg.StaffServisePort, err)
	}

	connBranch, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.BranchServiceHost, cfg.BranchServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("courier service dial host: %s port:%d err: %s",
			cfg.BranchServiceHost, cfg.BranchServisePort, err)
	}

	connCatalog, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.CatalogServiceHost, cfg.CatalogServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("courier service dial host: %s port:%d err: %s",
			cfg.CatalogServiceHost, cfg.CatalogServisePort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"branch_service":   branch_service.NewBranchServiceClient(connBranch),
			"staff_service":    staff_service.NewStaffServerClient(connStaff),
			"tarif_service":    staff_service.NewTarifServerClient(connStaff),
			"category_service": catalog_service.NewCategoryServiceClient(connCatalog),
			"product_service":  catalog_service.NewProductServiceClient(connCatalog),
		},
	}, nil
}

func (g *GrpcClient) BranchService() branch_service.BranchServiceClient {
	return g.connections["branch_service"].(branch_service.BranchServiceClient)
}

func (g *GrpcClient) TarifService() staff_service.TarifServerClient {
	return g.connections["tarif_service"].(staff_service.TarifServerClient)
}

func (g *GrpcClient) StaffService() staff_service.StaffServerClient {
	return g.connections["staff_service"].(staff_service.StaffServerClient)
}

// func (g *GrpcClient) CategoryService() catalog_service.CategoryServiceClient {
// 	return g.connections["category_service"].(catalog_service.CategoryServiceClient)
// }

// func (g *GrpcClient) ProductService() catalog_service.ProductServiceClient {
// 	return g.connections["product_service"].(catalog_service.ProductServiceClient)
// }
