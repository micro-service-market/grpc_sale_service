package grpc

import (
	"sale/genproto/sale_service"
	grpc_client "sale/grpc/client"
	"sale/grpc/service"
	"sale/packages/logger"
	"sale/storage"

	"google.golang.org/grpc"
)

func SetUpServer(log logger.LoggerI, strg storage.StoregeI, grpcClient grpc_client.GrpcClientI) *grpc.Server {
	s := grpc.NewServer()
	sale_service.RegisterSaleServerServer(s, service.NewSaleService(log, strg, grpcClient))
	sale_service.RegisterSaleProductServerServer(s, service.NewSaleProductService(log, strg, grpcClient))
	sale_service.RegisterBranchTransactionServerServer(s, service.NewBranchTransactionService(log, strg, grpcClient))
	sale_service.RegisterTransactionServerServer(s, service.NewTransactionService(log, strg, grpcClient))
	return s
}
